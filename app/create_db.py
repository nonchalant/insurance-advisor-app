import os

from elasticsearch import Elasticsearch

from config import db_path, db, app
from models import Query

# sample data
QUERIES = [
    {
        "title": "sample query 1",
        "description": "some longer description about banking",
        "tag": "Discovery Banking",
        "answer": "some answer to the banking query by a qualified professional",
        "contact_name": "Person 1",
        "contact_info": "",
        "contact_me": False,
        "done": True,
    },
    {
        "title": "sample query 2",
        "description": "some longer description about gap cover",
        "tag": "Discovery Gap cover",
        "answer": "some answer to the gap cover query by a qualified professional",
        "contact_name": "Person 2",
        "contact_info": "example@email.com",
        "contact_me": True,
        "contacted": False,
        "done": True,
    },
    {
        "title": "sample query 3",
        "description": "some longer description about Vitality",
        "tag": "Discovery Vitality",
        "answer": "some answer to the vitality query by a qualified professional",
        "contact_name": "Person 3",
        "contact_info": "082 333 4444",
        "contact_me": True,
        "contacted": True,
        "done": True,
    },
    {
        "title": "sample query 4",
        "description": "some longer description about credit cards",
        "tag": "Discovery Credit card",
        "answer": "some answer to the credit card query by a qualified professional",
        "contact_name": "Person 4",
        "contact_info": "some.email@example.com or 082 000 0011 (working hours only)",
        "contact_me": True,
        "contacted": False,
        "done": True,
    },
    {
        "title": "sample query 5",
        "description": "some longer description about medical aid",
        "tag": "Discovery Medical aid",
        "answer": None,
        "contact_name": "Person 4 again",
        "contact_info": "some.email@example.com or 082 000 0011 (working hours only)",
        "contact_me": True,
        "contacted": True,
        "done": False,
    },
]

# delete database file if it already exists
if os.path.exists(db_path):
    os.remove(db_path)

# create the database
db.create_all()

# iterate over the QUERIES structure and populate the database
for query in QUERIES:
    q = Query(
        title=query.get("title"),
        description=query.get("description"),
        tag=query.get("tag"),
        answer=query.get("answer"),
        contact_name=query.get("contact_name"),
        contact_info=query.get("contact_info"),
        contact_me=query.get("contact_me"),
        contacted=query.get("contact_me"),
        done=query.get("done"),
    )
    db.session.add(q)

# persist changes
db.session.commit()

# create initial search index
ELASTICSEARCH_URL = os.environ.get("ELASTICSEARCH_URL")
app.elasticsearch = Elasticsearch(ELASTICSEARCH_URL)
Query.reindex()
