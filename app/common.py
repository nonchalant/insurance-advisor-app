"""Functions that are common across modules"""
from datetime import datetime, timezone
from typing import List

from ago import human
from flask import make_response, jsonify, request

from config import db
from models import Query, ALLOWED_TAGS, QuerySchema


def build_search_results(result_set: List[Query], total: int):
    """Perform category search on Query table and return the result

    Expected response format::

        result = {
              "results": {
                "category1": {
                  "name": "Category 1",
                  "results": [
                    {
                      "title": "Result Title",
                      "url": "/optional/url/on/click",
                      "image": "optional-image.jpg",
                      "price": "Optional Price",
                      "description": "Optional Description"
                    },
                    {
                      "title": "Result Title",
                      "url": "/optional/url/on/click",
                      "image": "optional-image.jpg",
                      "price": "Optional Price",
                      "description": "Optional Description"
                    }
                  ]
                },
                "category2": {
                  "name": "Category 2",
                  "results": [
                    {
                      "title": "Result Title",
                      "url": "/optional/url/on/click",
                      "image": "optional-image.jpg",
                      "price": "Optional Price",
                      "description": "Optional Description"
                    }
                  ]
                }
              },
              "action": {
                "url": '/path/to/results',
                "text": "View all 202 results"
              }
            }

    :param total: total number of search results
    :param result_set: list of Query objects from search results
    :return: dictionary containing formatted results
    """
    # fetch all possible tags
    tags = [result.tag for result in result_set.all()]
    results = {}

    for tag in tags:
        results[tag] = {"name": tag, "results": []}

    # assign each result to a tag
    for result in result_set:
        formatted_result = {
            "title": result.title,
            "description": result.description,
            "url": f"/results/{result.tag}/{result.id}",
            "timestamp": result.timestamp,
        }
        results[result.tag]["results"].append(formatted_result)

    # format results
    results = {
        "results": results,
        "action": {"url": "/results/someurl", "text": f"View all {total} results"},
    }

    return results


def sanitize_request(json):
    response = {}
    for k, v in json.items():
        if v == "":
            response[k] = None
        else:
            response[k] = v

    if response.get("contact_me") == "on":
        response["contact_me"] = True
    elif response.get("contact_me") == "off":
        response["contact_me"] = False

    return response


def simplify_datetime(date_time=None):
    """
    Shortens an ugly datetime (utc ISO formatted) string

    Example input::
      '2019-03-01T13:10:40.870870+00:00'

    Example response::
      '2019-03-01 13:10'
    """
    date = str(datetime.fromisoformat(date_time).date())
    time = str(datetime.fromisoformat(date_time).replace(microsecond=0).time())
    dt_result = f"{date} {time}"

    return dt_result


def simplify_response_datetimes(response: List[Query]):
    """
    Converts timestamp fields in a query response for display in a table

    :param response: response containing a list of queries in the database
    :return: the same response, but with shortened datetime strings
    """
    if not isinstance(response, list):
        response = [response]

    result = []
    for query in response:
        try:
            timestamp = simplify_datetime(query["timestamp"])
        except Exception as e:
            print(f"Unsuccessful conversion of datetime to human-readable format.\n{e}")
            timestamp = query["timestamp"]

        query["timestamp"] = timestamp
        result.append(query)

    return result


def humanize_datetime(date_time=None):
    """
    Converts a datetime (utc ISO formatted) string to a human readable string

    Example input::
      '2019-03-01T13:10:40.870870+00:00'

    Example response::
      '21 hours, 26 minutes ago'
    """
    try:
        time = datetime.now(timezone.utc) - datetime.fromisoformat(date_time)
        time = human(time)
    except Exception as e:
        raise e

    return time


def humanize_response(response: List[Query]):
    """
    Converts timestamp fields in a query response to human readable format

    :param response: response containing a list of queries in the database
    :return: the same response, but with humanized timestamps
    """
    result = []
    for query in response:
        try:
            timestamp = humanize_datetime(query["timestamp"])
        except Exception as e:
            print(f"Unsuccessful conversion of datetime to human-readable format.\n{e}")
            timestamp = query["timestamp"]

        query["timestamp"] = timestamp
        result.append(query)

    return result


def clean_form_data(form):
    """
    Cleans form data received from data tables and returns a dict

    Original format::

      ImmutableMultiDict([
        ('action', 'edit'),
        ('data[6][contact_name]', 'Dexter'),
        ('data[6][answer]', 'There is no answer'),
        ('data[7][contact_name]', 'Megan'),
        ('data[7][answer]', 'There is an answer')
      ])

    Response::

    {
      "6":
          {
            "contact_name": "Dexter",
            "answer": "There is no answer"
          },
      "7":
          {
            "contact_name": "Megan",
            "answer": "There is an answer"
          }
    }

    :param form:
    :return:
    """
    form_dict = {}

    for k, v in form.items():
        id = k.split("data[")[-1].split("]")[0]
        key = k.split("][")[-1].replace("]", "")

        if not form_dict.get(id):
            form_dict[id] = {}

        form_dict[id][key] = v

    form_dict.pop("action", None)

    return form_dict


def format_errors(error_response, table_name=""):
    """
    Prepare Datatables error response from Marshmallow validation errors

    - Initial format of error_response::

        {
          'fieldErrors': [
            {
              'description': ['Not a valid email address.'],
              'date': ['Field may not be null.']
            }
          ]
        }

    - Return format::

        {
          "fieldErrors": [
            {
              "name": "first_name",
              "status": "This field is required"
            },
            {
              "name": "last_name",
              "status": "This field is required"
            }
          ]
        }

    :param error_response: nested dict describing field validation errors from Marshmallow
    :param table_name: name of the table to use when parsing table validation errors
    :return: nested dict describing the field validation errors in DataTables format
    """
    response = {"fieldErrors": []}

    for error in error_response["fieldErrors"]:
        for k, v in error.items():
            formatted_error = {"name": f"{table_name}.{k}", "status": v[0]}
            response["fieldErrors"].append(formatted_error)

    return response


def get_tag_options():
    """
    Build a list of tag options for Datatables 'select' field

    - Return format::

        [
          {
            "label": "Other product or service",
            "value": "Other product or service
          },
          {
            "label": "Discovery Banking",
            "value": "Discovery Banking"
          },
          {
            "label": "Discovery Invest",
            "value": "Discovery Invest"
          }
        ]

    :return: list of dicts mapping available tag options to selection labels
    """
    options = []

    for tag in ALLOWED_TAGS:
        option = {"label": tag, "value": tag}
        options.append(option)

    return options


def get_done_options():
    """
    Return boolean options for Datatables 'select' field

    - Return format::

        [
          {
            "label": "true",
            "value": True
          },
          {
            "label": "false",
            "value": False
          }
        ]

    :return: list of dicts mapping boolean values to selection labels
    """
    bool_dict = {"true": True, "false": False}
    options = []

    for l, v in bool_dict.items():
        option = {"label": l, "value": v}
        options.append(option)

    return options


def format_query_response(query_data):
    """
    Formats a query dict for consumption by Datatables

    :param query_data:
    :return:
    """
    if not isinstance(query_data, list):
        query_data = [query_data]

    response = [{"id": query["id"], "query": query} for query in query_data]
    options = {"query.tag": get_tag_options(), "query.done": get_done_options()}

    return {"data": response, "options": options}


def format_client_response(query_data):
    """
    Formats a client dict for consumption by Datatables

    :param query_data:
    :return:
    """
    results = []
    wanted_fields = [
        "contact_name",
        "contact_info",
        "contacted",
        "title",
        "description",
        "tag",
        "id",
    ]

    if not isinstance(query_data, list):
        query_data = [query_data]

    for query in query_data:
        result = {k: v for k, v in query.items() if k in wanted_fields}
        results.append(result)

    return {"data": results}


def create_query_from_json(json_data):
    # clean up request
    query_data = sanitize_request(json_data)

    # validate request against model
    schema = QuerySchema()
    query_data = schema.load(query_data, session=db.session).data

    # add validated request
    db.session.add(query_data)
    db.session.commit()

    # serialise the new query as a dict and build response
    query = schema.dump(query_data)
    response = {"queries": query}

    return make_response(jsonify(response), 201)


def create_query_from_form(form_data):
    # format form data for use in model validation
    form_data = clean_form_data(request.form)
    form_data = sanitize_request(form_data["0"])  # new Datatables entries are given "0"

    # prepare response
    success_response = {}
    error_response = {"fieldErrors": []}

    schema = QuerySchema()
    query = schema.load(form_data, session=db.session)

    if query.errors:
        error_response["fieldErrors"].append(query.errors)
    else:
        # add validated request
        db.session.add(query.data)
        db.session.commit()

        # serialise the new query as a dict and format response
        query_data = schema.dump(query.data).data
        query_data = simplify_response_datetimes(query_data)
        success_response = format_query_response(query_data)

    if error_response["fieldErrors"]:
        error_response = format_errors(error_response, table_name="query")
        response = make_response(jsonify(error_response), 409)
    else:
        response = make_response(jsonify(success_response), 201)

    return response
