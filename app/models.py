from datetime import datetime

from flask_login import UserMixin
from marshmallow import validate, fields
from werkzeug.security import generate_password_hash, check_password_hash

from config import db, ma, login
from search import add_to_index, remove_from_index, query_index

ALLOWED_TAGS = [
    "Other product or service",
    "Discovery Banking",
    "Discovery Car and home insurance",
    "Discovery Credit card",
    "Discovery Gap cover",
    "Discovery Investments",
    "Discovery Life insurance",
    "Discovery Medical aid",
    "Discovery Vitality",
]


@login.user_loader
def load_user(id):
    return Adviser.query.get(int(id))


class SearchableMixin(object):
    @classmethod
    def search(cls, expression, page, per_page):
        ids, total = query_index(cls.__tablename__, expression, page, per_page)
        if total == 0:
            return cls.query.filter_by(id=0), 0
        when = []
        for i in range(len(ids)):
            when.append((ids[i], i))
        return (
            cls.query.filter(cls.id.in_(ids)).order_by(db.case(when, value=cls.id)),
            total,
        )

    @classmethod
    def before_commit(cls, session):
        session._changes = {
            "add": list(session.new),
            "update": list(session.dirty),
            "delete": list(session.deleted),
        }

    @classmethod
    def after_commit(cls, session):
        for obj in session._changes["add"]:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes["update"]:
            if isinstance(obj, SearchableMixin):
                add_to_index(obj.__tablename__, obj)
        for obj in session._changes["delete"]:
            if isinstance(obj, SearchableMixin):
                remove_from_index(obj.__tablename__, obj)
        session._changes = None

    @classmethod
    def reindex(cls):
        for obj in cls.query:
            add_to_index(cls.__tablename__, obj)


# event listeners that will update the search index when changes are committed
db.event.listen(db.session, "before_commit", SearchableMixin.before_commit)
db.event.listen(db.session, "after_commit", SearchableMixin.after_commit)


class Adviser(UserMixin, db.Model):
    """
    Table containing insurance advisers with permission to answer queries

    - Fields::

        id [INT, PK]
        username [STR(64), UNIQ]
        email [STR(120), UNIQ]
        password_hash [STR(128)]
        is_admin [BOOL]

    """

    __tablename__ = "adviser"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    email = db.Column(db.String(120), index=True, unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    admin = db.Column(db.Boolean, default=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    answered_queries = db.relationship("Query", backref="adviser", lazy="dynamic")

    def __init__(self, username, email, confirmed, password_hash=None, admin=False):
        self.username = username
        self.email = email
        self.password_hash = password_hash
        self.admin = admin
        self.confirmed = confirmed

    def __repr__(self):
        """representation of the Adviser model"""
        return f"<Query(id={self.id}, username='{self.username}', email='{self.email}', admin={self.admin})>"

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Query(SearchableMixin, db.Model):
    """
    Table containing frequently asked questions and answers

    - Fields::

        id [INT, PK]
        title [STR(100)]
        description [STR(400)]
        tag [STR]
        answer [STR(400)]
        contact_name [STR(120)]
        contact_info [STR(120)]
        contact_me [BOOL]
        timestamp [DATETIME]
        done [BOOL]
        adviser_id [INT, FK]

    """

    __tablename__ = "query"
    __searchable__ = ["title", "description", "tag"]
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(100))
    description = db.Column(db.String(400))
    tag = db.Column(db.String, default="Other product or service")
    answer = db.Column(db.String(400))
    contact_name = db.Column(db.String(120))
    contact_info = db.Column(db.String(120))
    contact_me = db.Column(db.Boolean, default=False)
    contacted = db.Column(db.Boolean, default=False)
    timestamp = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow
    )
    done = db.Column(db.Boolean, default=False)
    adviser_id = db.Column(db.Integer, db.ForeignKey("adviser.id"), nullable=True)

    def __repr__(self):
        """representation of the Query model"""
        return f"<Query(id={self.id}, title='{self.title}', description='{self.description}', tag='{self.tag}', answer='{self.answer}', contact_name='{self.contact_name}', contact_info='{self.contact_info}', contact_me={self.contact_me}, timestamp={self.timestamp}, done={self.done}>"


class QuerySchema(ma.ModelSchema):
    """
    Check for the existence/validity of fields during validation when calling .load()
    """

    title = fields.String(required=True, validate=[validate.Length(max=100)])
    description = fields.String(required=True, validate=[validate.Length(max=400)])
    tag = fields.String(required=True, allow_none=True)
    answer = fields.String(
        required=False, allow_none=True, validate=[validate.Length(max=400)]
    )
    contact_name = fields.String(
        required=False, allow_none=True, validate=[validate.Length(max=120)]
    )
    contact_info = fields.String(
        required=False, allow_none=True, validate=[validate.Length(max=120)]
    )
    contact_me = fields.Boolean(required=False)
    timestamp = fields.DateTime(required=False, allow_none=True)
    done = fields.Boolean(required=False, allow_none=True)

    class Meta:
        model = Query
        sqla_session = db.session
