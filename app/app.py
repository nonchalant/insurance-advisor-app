from flask import (
    jsonify,
    abort,
    make_response,
    request,
    render_template,
    redirect,
    url_for,
    flash,
)
from flask_login import current_user, login_user, logout_user, login_required
from sqlalchemy import desc
from werkzeug.urls import url_parse

from common import (
    build_search_results,
    sanitize_request,
    humanize_response,
    clean_form_data,
    format_errors,
    simplify_response_datetimes,
    format_query_response,
    create_query_from_json,
    create_query_from_form,
    format_client_response,
)
from config import app, db, auth
from decorators import check_confirmed
from emails import send_email
from forms import LoginForm, RegistrationForm
from models import Adviser, Query, QuerySchema, ALLOWED_TAGS
from tokens import generate_confirmation_token, confirm_token


@auth.get_password
def get_password(username):
    if username == "davis":
        return "python"
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({"error": "Unauthorized access"}), 403)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({"error": "Not found"}), 404)


@app.route("/api/search", methods=["GET"])
def search_queries():
    term: str = request.args["query"]
    result, total = Query.search(term, 1, 8)
    result: dict = build_search_results(result, total)

    return jsonify(result)


@app.route("/api/clients", methods=["GET"])
@login_required
def get_clients():
    queries = (
        Query.query.filter(Query.contact_me.is_(True))
        .order_by(desc(Query.timestamp))
        .all()
    )

    schema = QuerySchema()
    query_data = schema.dump(queries, many=True).data

    # only retain desired fields
    results = []
    wanted_fields = [
        "contact_name",
        "contact_info",
        "contacted",
        "title",
        "description",
        "tag",
        "id",
    ]

    for query in query_data:
        result = {k: v for k, v in query.items() if k in wanted_fields}
        results.append(result)

    return jsonify({"data": results})


@app.route("/api/clients", methods=["PUT"])
@login_required
def update_clients():
    query_ids = request.args.get("id").split(",")

    success_response = {}
    error_response = {"fieldErrors": []}
    successful_queries = []

    for query_id in query_ids:
        query = Query.query.get(query_id)

        if not query:
            continue

        # serialise query as dict
        schema = QuerySchema()
        query_data = schema.dump(query).data

        # format form data for use in model validation
        form_data = clean_form_data(request.form)
        form_data = sanitize_request(form_data[query_id])

        # update query dict with new values
        query_data.update(form_data)

        # load updated Query instance
        schema = QuerySchema()
        query = schema.load(query_data, session=db.session)

        if query.errors:
            error_response["fieldErrors"].append(query.errors)
        else:
            # merge validated Query with existing
            db.session.merge(query.data)
            db.session.commit()

            # serialise the new query as a dict and format response
            query_data = schema.dump(query.data).data
            query_data = simplify_response_datetimes(query_data)
            successful_queries.extend(query_data)

    if error_response["fieldErrors"]:
        error_response = format_errors(error_response, table_name="query")
        response = make_response(jsonify(error_response), 409)
    else:
        success_response = format_client_response(successful_queries)
        response = make_response(jsonify(success_response), 201)

    return response


@app.route("/api/queries", methods=["GET"])
# @auth.login_required
@login_required
def get_queries():
    # get all queries
    queries = Query.query.order_by(Query.timestamp).all()
    schema = QuerySchema()
    query_data = schema.dump(queries, many=True).data
    query_data = simplify_response_datetimes(query_data)

    # format response
    response = format_query_response(query_data)

    return make_response(jsonify(response), 200)


@app.route("/api/queries/<int:query_id>", methods=["GET"])
def get_query(query_id):
    query = Query.query.get(query_id)

    if not query:
        abort(404)

    schema = QuerySchema()
    query_data = schema.dump(query)

    return jsonify({"queries": query_data})


@app.route("/api/queries", methods=["POST"])
def create_query():
    if request.form:
        response = create_query_from_form(request.form)
    elif request.json:
        response = create_query_from_json(request.json)
    else:
        abort(404)

    return response


@app.route("/api/queries", methods=["PUT"])
@login_required
def update_query():
    query_ids = request.args.get("id").split(",")

    success_response = {}
    error_response = {"fieldErrors": []}
    successful_queries = []

    for query_id in query_ids:
        query = Query.query.get(query_id)

        if not query:
            continue

        # serialise query as dict
        schema = QuerySchema()
        query_data = schema.dump(query).data

        # format form data for use in model validation
        form_data = clean_form_data(request.form)
        form_data = sanitize_request(form_data[query_id])

        # update query dict with new values
        query_data.update(form_data)

        # load updated Query instance
        schema = QuerySchema()
        query = schema.load(query_data, session=db.session)

        if query.errors:
            error_response["fieldErrors"].append(query.errors)
        else:
            # merge validated Query with existing
            db.session.merge(query.data)
            db.session.commit()

            # serialise the new query as a dict and format response
            query_data = schema.dump(query.data).data
            query_data = simplify_response_datetimes(query_data)
            successful_queries.extend(query_data)

    if error_response["fieldErrors"]:
        error_response = format_errors(error_response, table_name="query")
        response = make_response(jsonify(error_response), 409)
    else:
        success_response = format_query_response(successful_queries)
        response = make_response(jsonify(success_response), 201)

    return response


@app.route("/api/queries", methods=["DELETE"])
@login_required
def delete_query():
    query_ids = request.args.get("id").split(",")

    for query_id in query_ids:
        query = Query.query.get(query_id)

        if query:
            db.session.delete(query)
            db.session.commit()

    return jsonify({}, 200)


@app.route("/tag/<string:tag>", methods=["GET"])
def view_by_tag(tag):

    return f"success for {tag}"


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        if current_user.confirmed:
            return redirect(url_for("admin"))
        else:
            return redirect(url_for("unconfirmed"))
    form = LoginForm()
    if form.validate_on_submit():
        user = Adviser.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password", "error")
            return redirect(url_for("login"))
        login_user(user, remember=form.remember_me.data)
        # access_token = user.generate_token(user.id)
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("admin")
        return redirect(next_page)
    return render_template("login.html", form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("home"))


@app.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        if current_user.confirmed:
            return redirect(url_for("admin"))
        else:
            return redirect(url_for("unconfirmed"))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = Adviser(
            username=form.username.data, email=form.email.data, confirmed=False
        )
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        token = generate_confirmation_token(user.email)
        confirm_url = url_for("confirm_email", token=token, _external=True)
        html = render_template("_activate.html", confirm_url=confirm_url)
        subject = "Please confirm your email"
        send_email(user.email, subject, html)

        msg = "A confirmation link has been sent via email."
        flash(msg, "success")
        return redirect(url_for("login"))
    return render_template("register.html", form=form)


@app.route("/confirm/<token>")
@login_required
def confirm_email(token):
    try:
        email = confirm_token(token)
    except:
        flash("The confirmation link is invalid or has expired.", "error")
    user = Adviser.query.filter_by(email=email).first_or_404()
    if user.confirmed:
        flash("Account already confirmed. Please login.", "success")
    else:
        user.confirmed = True
        db.session.add(user)
        db.session.commit()
        flash(
            "Your email address has successfully been confirmed. "
            "Your application is under consideration by the advisory team.",
            "success",
        )
    return redirect(url_for("home"))


@app.route("/admin", methods=["GET", "PUT"])
@login_required
@check_confirmed
def admin():
    queries = (
        Query.query.filter(Query.done.is_(False)).order_by(desc(Query.timestamp)).all()
    )
    schema = QuerySchema()
    query_data = schema.dump(queries, many=True).data

    query_data = humanize_response(query_data)
    return render_template("admin.html", queries=query_data)


@app.route("/unconfirmed")
@login_required
def unconfirmed():
    if current_user.confirmed:
        return redirect(url_for("admin"))
    return render_template("_unconfirmed.html")


@app.route("/resend")
@login_required
def resend_confirmation():
    token = generate_confirmation_token(current_user.email)
    confirm_url = url_for("confirm_email", token=token, _external=True)
    html = render_template("_activate.html", confirm_url=confirm_url)
    subject = "Please confirm your email"
    send_email(current_user.email, subject, html)
    flash("A new confirmation email has been sent.", "success")
    return redirect(url_for("unconfirmed"))


@app.route("/contact")
def contact():
    # Todo: build out this page
    return render_template("contact.html")


@app.route("/account")
@login_required
@check_confirmed
def account():
    # Todo: build out this page
    return render_template("account.html")


@app.route("/")
@app.route("/index")
def home():
    queries = (
        Query.query.filter(Query.done.is_(True)).order_by(desc(Query.timestamp)).all()
    )
    schema = QuerySchema()
    query_data = schema.dump(queries, many=True).data

    query_data = humanize_response(query_data)

    return render_template("index.html", queries=query_data, tags=ALLOWED_TAGS)


if __name__ == "__main__":
    app.run()
