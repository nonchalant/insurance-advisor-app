# project/email.py

from threading import Thread

from flask_mail import Message

from config import app, mail


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(to, subject, template):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=app.config["MAIL_DEFAULT_SENDER"],
    )
    Thread(target=send_async_email, args=(app, msg)).start()
