import os

from elasticsearch import Elasticsearch
from flask import Flask
from flask_httpauth import HTTPBasicAuth
from flask_login import LoginManager
from flask_mail import Mail
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

# build the Sqlite URL for SqlAlchemy
basedir = os.path.abspath(os.path.dirname(__file__) + "/..")
db_name = "faq-db.sqlite"
db_path = os.path.join(basedir, db_name)
sqlite_url = "sqlite:////" + db_path

# configuration of the app instance
app = Flask(__name__, static_url_path="")
app.config["SQLALCHEMY_ECHO"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = sqlite_url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = os.environ.get("SECRET_KEY") or "?can-fish-walk-on-water?"
app.config["SECURITY_PASSWORD_SALT"] = (
    os.environ.get("SECURITY_PASSWORD_SALT") or "!fish-can-walk-on-water!"
)
app.config["ELASTICSEARCH_URL"] = os.environ.get("ELASTICSEARCH_URL")

# mail settings
app.config["MAIL_SERVER"] = "smtp.googlemail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_TLS"] = False
app.config["MAIL_USE_SSL"] = True

# gmail authentication
app.config["MAIL_USERNAME"] = os.environ["MAIL_USERNAME"]
app.config["MAIL_PASSWORD"] = os.environ["MAIL_PASSWORD"]

# mail accounts
app.config["MAIL_DEFAULT_SENDER"] = "from@example.com"

# add an elasticsearch attribute to the flask app
app.elasticsearch = (
    Elasticsearch([app.config["ELASTICSEARCH_URL"]])
    if app.config["ELASTICSEARCH_URL"]
    else None
)

# create the SqlAlchemy db instance
db = SQLAlchemy(app)

# add support for database migration
migrate = Migrate(app, db)

# add authentication
auth = HTTPBasicAuth()

# add support for flask login
login = LoginManager(app)
login.login_view = "login"  # where must a user be redirected to if not logged in

# initialize Marshmallow
ma = Marshmallow(app)

# initialise Mail
mail = Mail(app)
