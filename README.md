# insurance-advisor-app

A lightweight application for managing frequently asked questions for insurance brokers

## Elasticsearch

On an Ubuntu system, Elasticsearch can be installed by following [these instructions](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html).
After installing Elasticsearch, the service can be started with:
```
sudo systemctl start elasticsearch.service
```

To check that the service is running, visit the following address:
```
http://localhost:9200
```

To close the service, use:
```
sudo systemctl stop elasticsearch.service
```